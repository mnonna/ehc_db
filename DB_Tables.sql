CREATE TABLE clients(
	clientID INT NOT NULL AUTO_INCREMENT,
	clientName VARCHAR(30) NOT NULL,
    clientSurname VARCHAR(30) NOT NULL,
    e_mail VARCHAR(30),
    phoneNumber VARCHAR(9) NOT NULL,
    clientRegistrationTime DATETIME,
    address VARCHAR(120) NOT NULL,
    clientPESEL VARCHAR(11) NOT NULL UNIQUE,
    idCardNumber VARCHAR(8) UNIQUE,
    advancePaid BOOLEAN,
    PRIMARY KEY(clientID)
);
ALTER TABLE clients MODIFY COLUMN e_mail VARCHAR(60) NOT NULL UNIQUE;
ALTER TABLE clients MODIFY COLUMN address VARCHAR(10) NOT NULL;
ALTER TABLE clients ADD COLUMN street VARCHAR (30) NOT NULL AFTER clientRegistrationTime;
ALTER TABLE clients ADD COLUMN postalCode VARCHAR (6) NOT NULL AFTER address;
ALTER TABLE clients ADD COLUMN city VARCHAR (50) NOT NULL AFTER postalCode;

CREATE TABLE reservation(
	resID INT NOT NULL AUTO_INCREMENT,
    fk_clientID INT,
	fk_roomID SMALLINT,
    reservationStatus TINYINT,	#0-not active (but reserved) 1-active/in progress#
    reservationStart DATETIME,
    reservationEnd DATETIME,
    cost SMALLINT,
    advanceValue SMALLINT NOT NULL,
    advanceStatus TINYINT NOT NULL,
    peopleAmmount SMALLINT,
    PRIMARY KEY(resID), FOREIGN KEY(fk_clientID) REFERENCES clients(clientID)
    ON DELETE SET NULL
);

CREATE TABLE room(
	roomID SMALLINT NOT NULL AUTO_INCREMENT,
    fkR_clientID INT,
    floorNumber TINYINT,
    label TINYINT,
    fk_resID INT,
    roomStatus TINYINT,  # 0-free/not reserved, 1-reserved(but free), 2-occupied(during active reservation)#
    dayPrice SMALLINT,
    lastClean DATETIME,
    lastService DATETIME,
    isCleaningNeeded BOOLEAN,
    isServiceNeeded BOOLEAN,
    roomDescription VARCHAR(1000),
    PRIMARY KEY(roomID), FOREIGN KEY(fkR_clientID) REFERENCES clients(clientID) ON DELETE SET NULL
);
ALTER TABLE room MODIFY COLUMN label SMALLINT UNIQUE;
ALTER TABLE room MODIFY COLUMN dayPrice SMALLINT;
ALTER TABLE room ADD COLUMN capacity TINYINT AFTER roomDescription;

CREATE TABLE roomstate(
	stateID TINYINT,
    stateDesc VARCHAR(30),
    
    PRIMARY KEY(stateID)
);

INSERT INTO roomstate (stateID,stateDesc) VALUES(0,'FREE');
INSERT INTO roomstate (stateID,stateDesc) VALUES(1,'RESERVED');
INSERT INTO roomstate (stateID,stateDesc) VALUES(2,'OCCUPIED');

ALTER TABLE room ADD FOREIGN KEY (roomStatus) REFERENCES roomstate(stateID) ON DELETE SET NULL;

#Tablica pomocnicza zawierająca opisy uprawnień#
CREATE TABLE permissions(
	permissionID TINYINT AUTO_INCREMENT UNIQUE NOT NULL,
    permissionDescription VARCHAR(20),
  
    PRIMARY KEY (permissionID)
);

#Tablica pomocnicza zawierająca info o możliwych usługach dodatkowych#
CREATE TABLE services(
	serviceID TINYINT UNIQUE AUTO_INCREMENT,
	serviceType VARCHAR(20) NOT NULL,
    serviceTag VARCHAR(10) NOT NULL,
    #DLA USŁUG GODZINOWYCH#
	serviceHourPrice SMALLINT,
	
    PRIMARY KEY(serviceID)
);

CREATE TABLE itemservices(
	itserviceID TINYINT UNIQUE AUTO_INCREMENT,
	serviceType VARCHAR(20) NOT NULL,
    serviceTag VARCHAR(10) NOT NULL,
    #DLA USŁUG NA SZTUKI#
    serviceItemPrice SMALLINT,
    
    PRIMARY KEY(itserviceID)
);
INSERT INTO itemservices (serviceType,serviceTag,serviceItemPrice) VALUES ('Butelka szampana','VIP',250);


CREATE TABLE users(
	userID INT NOT NULL AUTO_INCREMENT,
    permissionGiven TINYINT,
    userLogin VARCHAR(60) UNIQUE,
    
    PRIMARY KEY (userID), FOREIGN KEY (permissionGiven) REFERENCES permissions(permissionID)
);

CREATE TABLE usersessions(
	sessionStart DATETIME,
    sessionEnd DATETIME,
	fk_userLogin VARCHAR(20),
    sessionID INT UNIQUE NOT NULL AUTO_INCREMENT,
    
    PRIMARY KEY (sessionID), FOREIGN KEY (fk_userLogin) REFERENCES users(userLogin)
);

SET foreign_key_checks = 0;
ALTER TABLE reservation ADD FOREIGN KEY (fk_roomID) REFERENCES room(roomID) ON DELETE CASCADE;
SET foreign_key_checks = 1;

ALTER TABLE room ADD COLUMN staffID INT AFTER lastService; 
ALTER TABLE room ADD FOREIGN KEY (staffID) REFERENCES users(userID);
################### 

#Tablica przechowująca usługi klientów#
CREATE TABLE clientsservices(
	addSerID INT NOT NULL AUTO_INCREMENT,
    clientID INT,
    serviceID TINYINT,
    
    serviceStart DATETIME,
    serviceEnd DATETIME,
    
    itemsNumber TINYINT,
    
    PRIMARY KEY(addSerID), 
    FOREIGN KEY(clientID) REFERENCES clients(clientID),
    FOREIGN KEY(serviceID) REFERENCES services(serviceID)
);
ALTER TABLE clientsservices ADD COLUMN fk_resID INT AFTER clientID;
ALTER TABLE clientsservices ADD FOREIGN KEY (fk_resID) REFERENCES reservation(resID) ON DELETE SET NULL;

ALTER TABLE clientsservices ADD COLUMN fk_itserviceID TINYINT AFTER serviceID;
ALTER TABLE clientsservices ADD FOREIGN KEY (fk_itserviceID) REFERENCES itemservices(itserviceID) ON DELETE CASCADE;
